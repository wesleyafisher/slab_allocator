CC = gcc
CCFLAGS = -g -Wall -Wextra -pedantic -O0 -std=c99
LDFLAGS =
OBJS = $(shell find . -name "*.c" -printf "%P " | sed 's/.c/.o/g')
EXEC = slabtest

%.o: %.c
	@echo "Creating Object $@"
	@$(CC) $(CCFLAGS) -c $< -o $@

$(EXEC): $(OBJS)
	@echo "Creating Executable $@"
	@$(CC) $(CCFLAGS) $(LDFLAGS) $(OBJS) -o $@

clean:
	@echo "Deleting Objects"
	@rm -rf $(OBJS)
	@echo "Deleting Executable"
	@rm -rf $(EXEC)
