int getFirstOpenSlab(void);
int getFirstOpenObject(int slabNum);

int getFreeMaskBit(int slabNum, int objectNum);
int getEmptyMaskBit(int slabNum);
int getPartialMaskBit(int slabNum);
int getFullMaskBit(int slabNum);

void setFreeMask(int slabNum, int objectNum, int bit);
void setEmptyMask(int slabNum, int bit);
void setPartialMask(int slabNum, int bit);
void setFullMask(int slabNum, int bit);
