#include "slab.h"
#include "slabHelper.h"

int getFirstOpenSlab(void){

  for(int i = 0; i < 16; i++){
    if(((full_mask >> (15-i)) & 1) == 0) return i;
  }

  return 73;

}

int getFirstOpenObject(int slabNum){

  for(int i = 1; i < 16; i++){
    if(((s[slabNum].free_mask >> (15-i)) & 1) == 1) return i;
  }

  return 73;

}

int getFreeMaskBit(int slabNum, int objectNum){

  return ((s[slabNum].free_mask) >> (15-objectNum)) & 1;

}

int getEmptyMaskBit(int slabNum){

  return (empty_mask >> (15-slabNum)) & 1;

}

int getPartialMaskBit(int slabNum){

  return (partial_mask >> (15-slabNum)) & 1;

}

int getFullMaskBit(int slabNum){

  return (full_mask >> (15-slabNum)) & 1;

}

void setFreeMask(int slabNum, int objectNum, int bit){

  if(objectNum < 1 || objectNum > 15){
    printf("setFreeMask() called on an invalid object_number!\n");
    return;
  }

  if(slabNum < 0 || slabNum > 15){
    printf("setFreeMask() called on an invalid slab_number!\n");
    return;
  }

  if(bit == 0){
    if(getFreeMaskBit(slabNum, objectNum) == 0){
      printf("Warning: pointless call to setFreeMask(0)\n");
      return;
    }
    s[slabNum].free_mask -= (1 << (15-objectNum));
  }

  else if(bit == 1){
    if(getFreeMaskBit(slabNum, objectNum) == 1){
      printf("Warning: pointless call to setFreeMask(1)\n");
      return;
    }
    s[slabNum].free_mask += (1 << (15-objectNum));
  }

  else{
    printf("setFreeMask() called to set an invalid bit!\n");
    return;
  }

}

void setEmptyMask(int slabNum, int bit){

  if(slabNum < 0 || slabNum > 15){
    printf("setEmptyMask() called on an invalid slab_number!\n");
    return;
  }

  if(bit == 0){
    if(getEmptyMaskBit(slabNum) == 0){
      printf("Warning: pointless call to setEmptyMask(0)\n");
      return;
    }
    empty_mask -= (1 << (15-slabNum));
  }

  else if(bit == 1){
    if(getEmptyMaskBit(slabNum) == 1){
      printf("Warning: pointless call to setEmptyMask(1)\n");
      return;
    }
    empty_mask += (1 << (15-slabNum));
  }

  else{
    printf("setEmptyMask() called to set an invalid bit!\n");
    return;
  }

}

void setPartialMask(int slabNum, int bit){

  if(slabNum < 0 || slabNum > 15){
    printf("setPartialMask() called on an invalid slab_number!\n");
    return;
  }

  if(bit == 0){
    if(getPartialMaskBit(slabNum) == 0){
      printf("Warning: pointless call to setPartialMask(0)\n");
      return;
    }
    partial_mask -= (1 << (15-slabNum));
  }

  else if(bit == 1){
    if(getPartialMaskBit(slabNum) == 1){
      printf("Warning: pointless call to setPartialMask(1)\n");
      return;
    }
    partial_mask += (1 << (15-slabNum));
  }

  else{
    printf("setPartialMask() called to set an invalid bit!\n");
    return;
  }

}

void setFullMask(int slabNum, int bit){

  if(slabNum < 0 || slabNum > 15){
    printf("setFullMask() called on an invalid slab_number!\n");
    return;
  }

  if(bit == 0){
    if(getFullMaskBit(slabNum) == 0){
      printf("Warning: pointless call to setFullMask(0)\n");
      return;
    }
    full_mask -= (1 << (15-slabNum));
  }

  else if(bit == 1){
    if(getFullMaskBit(slabNum) == 1){
      printf("Warning: pointless call to setFullMask(1)\n");
      return;
    }
    full_mask += (1 << (15-slabNum));
  }

  else{
    printf("setFullMask() called to set an invalid bit!\n");
    return;
  }

}
