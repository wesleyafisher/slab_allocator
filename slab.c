#include "slab.h"
#include "slabHelper.h"

unsigned char *slab_allocate(){

	//Can't allocate a slab if there is no space left!
	if(full_mask == 0xffff) return NULL;

	//Find the first slab that has space in it
	int slab_number = getFirstOpenSlab(); //0-15
	if(slab_number == 73)
		printf("1: getFirstOpenSlab() did not find an open slab!\n");
	else if(s[slab_number].free_count == 0)
		printf("2: getFirstOpenSlab() did not get an open slab!\n");

	//Find the first open object in the slab
	int object_number = getFirstOpenObject(slab_number); //1-15
	if(object_number == 73)
		printf("1: getFirstOpenObject() did not find an open object!\n");
	else if(getFreeMaskBit(slab_number, object_number) == 0)
		printf("2: getFirstOpenObject() did not get an open object!\n");

	//Decrement the free count
	s[slab_number].free_count--;

	//Set bit masks
	setFreeMask(slab_number, object_number, 0);
	if(s[slab_number].free_count == 14){
		setEmptyMask(slab_number, 0);
		setPartialMask(slab_number, 1);
	}
	else if(s[slab_number].free_count == 0){
		setPartialMask(slab_number, 0);
		setFullMask(slab_number, 1);
	}

	return start + (slab_number * 0x1000) + (object_number * 0x0100);

}

int slab_release( unsigned char *addr ){

	//Check for an invalid address
	if(addr < start || addr > (start + 0xff00)){
		//printf("Address is outside of valid range!\n");
		return 1;
	}

	if((addr-start) % 0x0100 != 0){
		//printf("Address is not aligned to an object!\n");
		return 1;
	}

	//Check for invalid signature
	int slab_number = (addr - start) >> 12;

	if(slab_number < 0 || slab_number > 15){
		printf("Invalid slab number!\n");
		return 1;
	}

	if(s[slab_number].signature != 0x51ab51ab){
		//printf("Slab does not have correct signature!\n");
		return 1;
	}

	//Check that object is not already marked as free
	int object_number = ((addr - start) >> 8) & 0x000f;

	if(getFreeMaskBit(slab_number, object_number) == 1){
		//printf("This object is already labeled as free!\n");
		return 2;
	}

	//Increment the free count
	s[slab_number].free_count++;

	//Set bit masks
	setFreeMask(slab_number, object_number, 1);
	if(s[slab_number].free_count == 15){
		setEmptyMask(slab_number, 1);
		setPartialMask(slab_number, 0);
	}
	else if(s[slab_number].free_count == 1){
		setPartialMask(slab_number, 1);
		setFullMask(slab_number, 0);
	}

	return 0;

}
